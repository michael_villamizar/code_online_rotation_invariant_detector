
  Fast Online Learning and Detection of Natural Landmarks for Autonomous Aerial Robots

  Description:
    This file contains the code to perform online learning and detection of
    natural landmarks for aerial robotics applications [1]. More specifically, this
    code allows to learn and detect simultaneously a visual target under in-plane
    rotations. Initially, the human selects via the computer's mouse the target in
    the image that he/she wants to learn and recognize in future frames.
    Subsequently, the classifier is initially computed using a set of training
    samples generated artificially and for multiple orientations. Positive samples
    are extracted using random shift transformations over the target, whereas negative
    samples are random patches from the background.

    In run time, the classifier detects the visual landmark and predicts its planar
    orientation. The classifier uses its own detection predictions to update and
    refine the target appearance model (self-learning). However, in cases where
    the classifier is uncertain about its output (sample label), the classifier
    is not updated in order to reduce the risk of drifting. This is controlled using
    an uncertain threshold parameter (omega).

    If you make use of this code for research articles, we kindly encourage
    to cite the reference [1], listed below. This code is only for research
    and educational purposes.

  Requirements:
    1. opencv (e.g opencv 2.4.9)
    2. cmake

  Compilation:
    1. mkdir build
    2. cd build
    3. cmake ..
    3. make
    4. ./detector

  Comments:
    1. The program works at any input image resolution. However, the screen
       information like score, results, etc, are displayed for a resolution
       of 640x480 pixels. This is the default resolution. If you change the
       resolution you must sure that the functions are shown properly. We
       recommend not using a resolution lower of 640x480 pixels.
    2. The program parameters are defined in the file parameters.txt located
       in the files folder. In this file you can change the detector parameters
       and program funcionalities.
    3. The code provided corresponds to the online random ferns classifier without
       using the feature selection step proposed in the article [1]. For this case, 
       the features are chosen completely at random. The feature selection step,
       based on information gain, is pending of release. 

  Keyboard commands:
    p: pause the video stream
    ESC: exit the program
    space: enable/disable object detection
    l: decrease online the detector threshold
    h: increase online the detector threshold

  Contact:
    Michael Villamizar
    mvillami@iri.upc.edu
    Institut de Robòtica i Informàtica Industrial, CSIC-UPC
    Barcelona - Spain
    2015

  References:
    [1] Fast Online Learning and Detection of Natural Landmarks for Autonomous Aerial Robots
        M. Villamizar, A. Sanfeliu and F. Moreno-Noguer
       International Conference on Robotics and Automation (ICRA), 2014


