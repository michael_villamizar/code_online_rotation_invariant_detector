#ifndef detector_functions_h
#define detector_functions_h

#include "detector_functions.h"
#include "detector_classes.h"

using namespace std;


// save image
void fun_save_image(cv::Mat img, long int number);

// draw message
void fun_draw_message(clsParameters* prms, cv::Mat img, char* text, CvPoint location, CvScalar textColor);

// show frame
void fun_show_frame(clsParameters* prms, cv::Mat img, int contFrame, double FPS);

// show image patch
void fun_show_image_patch(clsParameters* prms, cv::Mat img, clsDetectionSet* detSet);

// image equalization
void fun_image_equalization(cv::Mat img);

// detection labels
cv::Mat fun_detection_labels(clsParameters* prms, clsDetectionSet* detSet);

// draw detection mode
void fun_draw_detection_mode(cv::Mat img, bool status);

// draw detection score
void fun_draw_detection_score(clsParameters*prms, cv::Mat img, clsDetectionSet* detSet);

// draw detection threshold
void fun_draw_detection_threshold(clsParameters*prms, cv::Mat img, float threshold);

// draw frame data
void fun_draw_frame_data(clsParameters* prms, cv::Mat img, clsFrameData &data);

// frame data
void fun_frame_data(clsDetectionSet* detSet, cv::Mat labels, clsFrameData &frmData);

// test classifier
void fun_test_classifier(clsClassifier* clfr, cv::Mat img, clsDetectionSet* detSet, CvRect area, int scale);

// detection
void fun_detect(clsParameters* prms, clsClassifier* clfr, cv::Mat img, clsDetROI ROI, clsDetectionSet* detSet);

// update classifier using positive samples
void fun_update_positive_samples(clsClassifier* clfr, cv::Mat img, CvRect box, int pose, int numSamples, float shift);

// update classifier using negative samples
void fun_update_negative_samples(clsClassifier* clfr, cv::Mat img, CvRect box, int pose, int numSamples, float shift);

// update classifier
void fun_update_classifier(clsParameters* prms, clsClassifier* clfr, cv::Mat img, clsDetectionSet* detSet, cv::Mat labels);

// train classifier
void fun_train_classifier(clsParameters* prms, clsClassifier* clfr, cv::Mat img, CvRect box);

#endif
