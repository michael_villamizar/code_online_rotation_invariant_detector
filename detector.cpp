/*

  Fast Online Learning and Detection of Natural Landmarks for Autonomous Aerial Robots

  Description:
    This file contains the code to perform online learning and detection of
    natural landmarks for aerial robotics applications [1]. More specifically, this
    code allows to learn and detect simultaneously a visual target under in-plane
    rotations. Initially, the human selects via the computer's mouse the target in
    the image that he/she wants to learn and recognize in future frames.
    Subsequently, the classifier is initially computed using a set of training
    samples generated artificially and for multiple orientations. Positive samples
    are extracted using random shift transformations over the target, whereas negative
    samples are random patches from the background.

    In run time, the classifier detects the visual landmark and predicts its planar
    orientation. The classifier uses its own detection predictions to update and
    refine the target appearance model (self-learning). However, in cases where
    the classifier is uncertain about its output (sample label), the classifier
    is not updated in order to reduce the risk of drifting. This is controlled using
    an uncertain threshold parameter (omega).

    If you make use of this code for research articles, we kindly encourage
    to cite the reference [1], listed below. This code is only for research
    and educational purposes.

  Requirements:
    1. opencv (e.g opencv 2.4.9)
    2. cmake

  Compilation:
    1. mkdir build
    2. cd build
    3. cmake ..
    3. make
    4. ./detector

  Comments:
    1. The program works at any input image resolution. However, the screen
       information like score, results, etc, are displayed for a resolution
       of 640x480 pixels. This is the default resolution. If you change the
       resolution you must sure that the functions are shown properly. We
       recommend not using a resolution lower of 640x480 pixels.
   2. The program parameters are defined in the file parameters.txt located
      in the files folder. In this file you can change the detector parameters
      and program funcionalities.

  Keyboard commands:
    p: pause the video stream
    ESC: exit the program
    space: enable/disable object detection
    l: decrease online the detector threshold
    h: increase online the detector threshold

  Contact:
    Michael Villamizar
    mvillami@iri.upc.edu
    Institut de Robòtica i Informàtica Industrial, CSIC-UPC
    Barcelona - Spain
    2015

  References:
    [1] Fast Online Learning and Detection of Natural Landmarks for Autonomous Aerial Robots
        M. Villamizar, A. Sanfeliu and F. Moreno-Noguer
       International Conference on Robotics and Automation (ICRA), 2014

*/

#include <list>
#include <time.h>
#include <string>
#include <stdio.h>
#include <iostream>
#include <signal.h>
#include "detector_classes.h"
#include "detector_functions.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

using namespace std;

// global variables
int rad = 0;  // circle radius (target)
bool flg_exit = false;  // exit program
bool flg_train = false;  // enable training
bool flg_pause = false;  // pause the program
bool flg_drawing = false;  // enable drawing circle
bool flg_detection = false;  // enable detection
CvPoint ctr = cvPoint(0,0);  // circle center point (target)
CvRect rec = cvRect(-1,-1,0,0);  // bounding box (target)

// functions
void fun_mouse_callback(int event, int x, int y, int flags, void* param);
void fun_keyboard_callback(clsParameters* prms, cv::Mat &img, char key);

// main function
int main( int argc, char* argv[] ){

  // variables
  char key;  // keyboard key
  int numDets;  // num. detections
  float detThr;  // detection threshold
  cv::Mat frame;  // video frame
  clsDetROI ROI;  // detection region of interest
  CvScalar color;  // color
  int contFPS = 0;  // frames per second counter
  time_t start,end;  // times
  int height,width;  // image heigth and width
  double dif,FPS = 0;  // diff. time and frames per second
  long int contFrame = 0;  // frame counter

  // program parameters
  clsParameters* prms = new clsParameters;  // initialize paramaters
  prms->fun_load();  // load parameters from file
  prms->fun_print();  // print program parameters

  // object classifier
  clsClassifier* clfr = new clsClassifier;  // initialize the classifier

  // image size
  width = prms->fun_get_image_width();  // width
  height = prms->fun_get_image_height();  // height

  // default color and font size
  color = prms->fun_get_color();

  // video capture
  cv::VideoCapture capture(0); // webcam

  // check if we succeeded
  if(!capture.isOpened()){ 
    cout << "Couldn't capture stream" << endl;
    return -1;
  }

  // window
  cv::namedWindow("detection", CV_WINDOW_AUTOSIZE);

  // initial clock
  time(&start);

  // stream
  while(1){

    // temporal variables
    clsFrameData frmData;  // frame data
    clsDetectionSet* detSet = new clsDetectionSet; // object detections set

    // video frame
    if (!flg_pause)
      capture >> frame; 

    // create and resize input image
    cv::Mat img = frame;
    cv::resize(img, img, cvSize(width,height));

    // image equalization
    if (prms->fun_image_equalization()==1)
      fun_image_equalization(img);

    // bounding box
    cv::setMouseCallback("detection", fun_mouse_callback, &img);

    // show object target (circle)
    if (flg_drawing){
      fun_draw_message(prms, img, "bounding box", cvPoint(10, height-30), color);
      cv::circle(img, ctr, rad, color, 3, 8, 0);
    }

    // check stop
    if (!flg_pause){

      // train step
      if (flg_train){

        // set/reset variable
        flg_train = false;
        flg_detection = true;

        // train classifier
        fun_train_classifier(prms, clfr, img, rec);

      }

      // check detection
      if (flg_detection){

        // draw detection mode: detection or "tracking"
        fun_draw_detection_mode(img, ROI.fun_get_status());

        // object detection using the detection ROI -prior localization-
        fun_detect(prms, clfr, img, ROI, detSet);

        // num. detections
        numDets = detSet->fun_get_num_detections();

        // reset detection ROI
        ROI.fun_clear();

        // check
        if (numDets>0){

          // detection labels
          cv::Mat labels = fun_detection_labels(prms, detSet);

          // update classifier
          fun_update_classifier(prms, clfr, img, detSet, labels);

          // frame data
          fun_frame_data(detSet, labels, frmData);

          // max. detection -search area-
          if (frmData.boxes.size()==1)
            ROI.fun_set_values(frmData.boxes.back(),frmData.scales.back());

        }

        // max. detection score, detection threshold and image patch
        fun_draw_detection_score(prms, img, detSet);
        fun_draw_detection_threshold(prms, img, prms->fun_get_threshold());

      }

      // update counters
      contFPS++;
      contFrame++;

      // times
      time(&end);
      dif = difftime(end,start);
      if (dif>=1.){
        FPS = contFPS/dif;
        printf("Frame count -> %ld : FPS -> %.4f \n",contFrame, FPS);
        contFPS = 0;
        time(&start);
      }

      // draw frame data
      fun_draw_frame_data(prms, img, frmData);

      // show frame message
      fun_show_frame(prms, img, contFrame, FPS);

      // save image
      if (prms->fun_save_images()==1)
        fun_save_image(img, contFrame);

    }

    // show image
    cv::imshow("detection", img);
    key = cv::waitKey(1);

    // keyboard commands
    fun_keyboard_callback(prms, img, key);

    // release
    delete detSet;

    // break point
    if (flg_exit) break;

  }

  // release
  delete prms,clfr;
  cv::destroyWindow("detection");
  return 0;
}

// mouse callback
void fun_mouse_callback(int event, int x, int y, int flags, void* param){

  // input image
  cv::Mat* img = (cv::Mat*) param;

  // events
  switch (event){
    // mouse move
    case CV_EVENT_MOUSEMOVE:{
      if (flg_drawing){

        // circle radius
        rad = sqrt((x - ctr.x)*(x - ctr.x) + (y - ctr.y)*(y - ctr.y));

        // bounding box
        rec.x = ctr.x - rad;
        rec.y = ctr.y - rad;
        rec.width = 2*rad;
        rec.height = 2*rad;
      }
    }
    break;

    // left button down
    case CV_EVENT_LBUTTONDOWN:{

      // enable drawing target
      flg_drawing = true;

      // center point (target)
      ctr.x = x;
      ctr.y = y;
    }
    break;

    // left button up
    case CV_EVENT_LBUTTONUP:{

      // enable training
      flg_drawing = false;
      flg_train = true;

      // circle radius
      rad = sqrt((x - ctr.x)*(x - ctr.x) + (y - ctr.y)*(y - ctr.y));

      // bounding box
      rec.x = ctr.x - rad;
      rec.y = ctr.y - rad;
      rec.width = 2*rad;
      rec.height = 2*rad;

    }
    break;

  }
};

// keyboard callback
void fun_keyboard_callback(clsParameters* prms, cv::Mat &img, char key){

  // variables
  float detThr;  // detection threshold
  float step = 0.02;  // threshold step

  // keys actions
  switch (key){

    // ESC -> exit
    case 27:
      // message
      printf("\n!! EXIT\n\n");
      // exit program
      flg_exit = true;
      break;

    // space -> detection
    case 32:
      if (flg_detection){
        // switch off detection
        flg_detection = false;
      }
      else{
        // message
        printf("\n!! Detection\n\n");
        // enable detection
        flg_detection = true;
      }
      break;

    // h -> increase the detection threshold
    case 104:
      // current threshold
      detThr = prms->fun_get_threshold();
      // set new threshold value
      prms->fun_set_threshold(detThr + step);
      // message
      printf("\n!! detection threshold -> %.2f \n\n",detThr + step);
      break;

    // l -> decrease the detection threshold
    case 108:
      // current threshold
      detThr = prms->fun_get_threshold();
      // set new threshold value
      prms->fun_set_threshold(detThr - step);
      // message
      printf("\n!! detection threshold -> %.2f \n\n",detThr - step);
      break;

    // p -> pause
    case 112:
      if (flg_pause){
      flg_pause = false;
    }
    else{
      printf("!! PAUSE\n");
      flg_pause = true;
      fun_draw_message(prms, img, "PAUSE", cvPoint(10, 30), cvScalar(255, 255, 0));
    }
  }
};

