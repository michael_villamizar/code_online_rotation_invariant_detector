#include <list>
#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "detector_classes.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

#define PI 3.14159265
using namespace std;

// clsParameters
// constructor
clsParameters::clsParameters(){
  // initialize default parameters
  fun_initialize();
};
// destructor
clsParameters::~clsParameters(){
};
// initialize default parameters
void clsParameters::fun_initialize(){

  // default values
  this->objSize = 20;  // object size (squared)
  this->imgWidth = 640;  // image width
  this->imgHeight = 480;  // image height
  this->numPoses = 1;  // num. object poses
  this->numFerns = 100;  // num. random ferns
  this->numFeatures = 8;  // num. features
  this->threshold = 0.58;  // classifier threshold
  this->omega = 0.2;  // assistance threshold
  this->recThick = 4;  // rectangle thickness
  this->imgShift = 0.15;  // image shift
  this->minCellSize = 5;  // min. image cell size
  this->maxCellSize = 20;  // max. image cell size
  this->numTrnSamples = 200;  // num. training samples
  this->numNewSamples = 2;  // num. new (boostrapped) samples
  this->font = 2.0;  // text font
  this->patchSize = 50;  // patch size
  this->imgEqualization = 1;  // image equalization
  this->saveImages = 0;  // save images
  this->color = cvScalar(0,255,0);  // rectangle color
  this->filePath = "../files/parameters.txt"; // parameters path

};
// get num. ferns
int clsParameters::fun_get_num_ferns(){
  return this->numFerns;
};
// get num. features
int clsParameters::fun_get_num_features(){
  return this->numFeatures;
};
// get num. object poses
int clsParameters::fun_get_num_poses(){
  return this->numPoses;
};
// get object size
int clsParameters::fun_get_object_size(){
  return this->objSize;
};
// get threshold
float clsParameters::fun_get_threshold(){
  return this->threshold;
};
// set threshold
void clsParameters::fun_set_threshold(float thr){
  this->threshold = thr;
};
// get assistance threshold
float clsParameters::fun_get_assistance_threshold(){
  return this->omega;
};
// get min. cell size
int clsParameters::fun_get_min_cell_size(){
  return this->minCellSize;
};
// get max. cell size
int clsParameters::fun_get_max_cell_size(){
  return this->maxCellSize;
};
// set parameters file path
void clsParameters::fun_set_parameters_path(char* path){
  this->filePath = path;
};
// get the number of samples
int clsParameters::fun_get_num_train_samples(){
  return this->numTrnSamples;
};
// get the number of new samples
int clsParameters::fun_get_num_new_samples(){
  return this->numNewSamples;
};
// get image width
int clsParameters::fun_get_image_width(){
  return this->imgWidth;
};
// get image height
int clsParameters::fun_get_image_height(){
  return this->imgHeight;
};
// get image shift
float clsParameters::fun_get_image_shift(){
  return this->imgShift;
};
// get patch size
int clsParameters::fun_get_patch_size(){
  return this->patchSize;
};
// get thickness
int clsParameters::fun_get_rectangle_thickness(){
  return this->recThick;
};
// get color
CvScalar clsParameters::fun_get_color(){
  return this->color;
};
// get text font
float clsParameters::fun_get_font(){
  return this->font;
};
// get image equalization
int clsParameters::fun_image_equalization(){
  return this->imgEqualization;
};
// get save images
int clsParameters::fun_save_images(){
  return this->saveImages;
};
// load parameters
void clsParameters::fun_load(){

  // variables
  char buffer[128];
  FILE *ptr = NULL;
  int fontThick = 3;

  // open file
  if ((ptr = fopen(this->filePath, "r"))!= NULL){

    // read parameters
    fgets(buffer, 128, ptr);
    fscanf(ptr, "%s", buffer);
    fscanf(ptr, "%s %d", buffer, &this->imgWidth);  // image width
    fscanf(ptr, "%s %d", buffer, &this->imgHeight);  // image height
    fscanf(ptr, "%s %d", buffer, &this->objSize);  // object width
    fscanf(ptr, "%s %d", buffer, &this->numPoses);  // num. poses
    fscanf(ptr, "%s %d", buffer, &this->numFerns);  // num. shared random ferns
    fscanf(ptr, "%s %d", buffer, &this->numFeatures);  // num. binary features per fern
    fscanf(ptr, "%s %f", buffer, &this->threshold);  // classifier threshold
    fscanf(ptr, "%s %f", buffer, &this->omega);  //  human assistance threshold
    fscanf(ptr, "%s %d", buffer, &this->minCellSize);  // min. cell size -pyramide image-
    fscanf(ptr, "%s %d", buffer, &this->maxCellSize);  // max. cell size -pyramide image-
    fscanf(ptr, "%s %f", buffer, &this->imgShift);  // image shift factor
    fscanf(ptr, "%s %d", buffer, &this->numTrnSamples);  // num. training samples
    fscanf(ptr, "%s %d", buffer, &this->numNewSamples);  // num. bootstrapped samples
    fscanf(ptr, "%s %d", buffer, &this->patchSize);  // object patch size
    fscanf(ptr, "%s %d", buffer, &this->imgEqualization);  // image equalization
    fscanf(ptr, "%s %d", buffer, &this->saveImages);  // saveImages
    fscanf(ptr, "%s %d", buffer, &this->recThick);  // detection rectangle thickness
    fscanf(ptr, "%s %f", buffer, &this->font);  // text font size

    // set color
    this->color = cvScalar(0,255,0);

    // close file pointer
    fclose(ptr);

  }
  else {
    cout << "ERROR : not input parameter file" << endl;
    exit(0);
  }
};
// print parameters
void clsParameters::fun_print(){

  cout << "\n*********************************************************" << endl;
  cout << "*   Fast Online Learning and Detection of Natural       *" << endl;
  cout << "*       Landmarks for Autonomous Aerial Robots          *" << endl;
  cout << "*                    Michael Villamizar                 *" << endl;
  cout << "*                 mvillami.at.iri.upc.edu               *" << endl;
  cout << "*                          2015                         *" << endl;
  cout << "*********************************************************" << endl;

  cout << "\n*********************************************************" << endl;
  cout << "* Program parameters :" << endl;
  cout << "* image size -> " << this->imgHeight << "x" << this->imgWidth << endl;
  cout << "* object size -> " << this->objSize << endl;
  cout << "* num. random ferns -> " << this->numFerns << endl;
  cout << "* num. features -> " << this->numFeatures << endl;
  cout << "* threshold -> " << this->threshold << endl;
  cout << "* omega -> " << this->omega << endl;
  cout << "* num. training samples -> " << this->numTrnSamples << endl;
  cout << "* num. new samples -> " << this->numNewSamples << endl;
  cout << "* min. cell size -> " << this->minCellSize << endl;
  cout << "* max. cell size -> " << this->maxCellSize << endl;
  cout << "* image shift -> " << this->imgShift << endl;
  cout << "* rec. thickness -> " << this->recThick<< endl;
  cout << "*********************************************************" << endl;

};




// clsClassifier
// constructor
clsClassifier::clsClassifier(){
  // initialize default parameters
  fun_initialize();
};
// destructor
clsClassifier::~clsClassifier(){
};
// initialize with default values
void clsClassifier::fun_initialize(){
  // default parameters
  this->objSize = 24;  // object size (squared)
  this->numBins = 128;  // number of histogram bins (7 features)
  this->numFerns = 100;  // number of random ferns
  this->numFeats = 7;  // number of fern features
  this->numPoses = 1;  // number of classifier poses (object poses)
  this->numChans = 3;  // number of feature channels
  this->threshold = 0.5;  // classifier threshold
  this->filePath = "../files/detector.txt"; // parameters path
};
// get object size
int clsClassifier::fun_get_object_size(){
  return this->objSize;
};
// get num. random ferns
int clsClassifier::fun_get_num_ferns(){
  return this->numFerns;
};
// get num. features
int clsClassifier::fun_get_num_features(){
  return this->numFeats;
};
// get num. object poses
int clsClassifier::fun_get_num_poses(){
  return this->numPoses;
};
// get num. bins
int clsClassifier::fun_get_num_bins(){
  return this->numBins;
};
// get num. feature channels
int clsClassifier::fun_get_num_channels(){
  return this->numChans;
};
// get threshold
float clsClassifier::fun_get_threshold(){
  return this->threshold;
};
// set threshold
void clsClassifier::fun_set_threshold(float thr){
  this->threshold = thr;
};
// set detector file path
void clsClassifier::fun_set_detector_path(char* path){
  this->filePath = path;
};
// get random ferns
cv::Mat clsClassifier::fun_get_ferns(){
  return this->ferns;
};
// get positive fern distributions
cv::Mat clsClassifier::fun_get_posHstms(){
  return this->posHstms;
};
// get negative fern distributions
cv::Mat clsClassifier::fun_get_negHstms(){
  return this->negHstms;
};
// get ratio of fern distributions
cv::Mat clsClassifier::fun_get_ratHstms(){
  return this->ratHstms;
};
// compute the classifier
void clsClassifier::fun_compute(int numFerns, int numFeats, int numPoses, int objSize){

  // variables
  float rad,ang;
  int rxa,rxb,rya,ryb,depth;
  int xa,ya,za,xb,yb,zb,ctr,poseIndx;

  // set parameters
  this->objSize = objSize;  // object size
  this->numFerns = numFerns;  // number of random ferns
  this->numFeats = numFeats;  // number of fern features
  this->numPoses = numPoses;  // number of classifier poses
  this->numBins = (int)pow(2,numFeats);  // number of histogram bins

  // center point (x and y)
  ctr = round(this->objSize/2);

  // fern data depth (channels): rotated ferns copies to keep efficiency
  depth = 6*numPoses;

  // weak classifiers
  this->ferns = cv::Mat(this->numFerns, this->numFeats, CV_8UC(depth));

  // pointer to ferns data
  unsigned char *fernsPtr = (unsigned char*)(this->ferns.data);

  // random ferns
  for (int fern = 0; fern<this->numFerns; fern++ ){
    for (int feat = 0; feat<this->numFeats; feat++){

      // check feature inside circle (for rotation invariant)
      rad = this->objSize;
      while (rad>this->objSize/2){

        // feature coordinates
        ya = floor(this->objSize*((double)rand()/RAND_MAX));
        xa = floor(this->objSize*((double)rand()/RAND_MAX));
        za = floor(this->numChans*((double)rand()/RAND_MAX));

        // radius
        rad = sqrt((ya-ctr)*(ya-ctr) + (xa-ctr)*(xa-ctr));
      }

      // check feature inside circle (for rotation invariant)
      rad = objSize;
      while (rad>objSize/2){

        // feature coordinates
        yb = floor(this->objSize*((double)rand()/RAND_MAX));
        xb = floor(this->objSize*((double)rand()/RAND_MAX));
        zb = floor(this->numChans*((double)rand()/RAND_MAX));

        // radius
        rad = sqrt((yb-ctr)*(yb-ctr) + (xb-ctr)*(xb-ctr));
      }

      // random ferns for multiple object poses -planar rotations-
      for (int p = 0; p<numPoses; p++){

        // rotation angle
        ang = p*2*PI/numPoses;

        // pose depth index
        poseIndx = p*6;

        // fern rotation -spatial coordinates-
        rxa = ctr + (xa-ctr)*cos(ang) -  (ya-ctr)*sin(ang);
        rya = ctr + (xa-ctr)*sin(ang) +  (ya-ctr)*cos(ang);
        rxb = ctr + (xb-ctr)*cos(ang) -  (yb-ctr)*sin(ang);
        ryb = ctr + (xb-ctr)*sin(ang) +  (yb-ctr)*cos(ang);

       // save random ferns
       *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+0)) = rya;
       *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+1)) = rxa;
       *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+2)) = za;
       *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+3)) = ryb;
       *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+4)) = rxb;
       *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+5)) = zb;

      }
    }
  }

  // distributions
  this->posHstms = cv::Mat(this->numFerns, this->numBins, CV_32FC(this->numPoses));
  this->negHstms = cv::Mat(this->numFerns, this->numBins, CV_32FC(this->numPoses));
  this->ratHstms = cv::Mat(this->numFerns, this->numBins, CV_32FC(this->numPoses));

};
// update the classifier
void clsClassifier::fun_update(cv::Mat &img, float label){

  // variables
  float valA,valB,pos,neg;
  int tmpF,tmpW,tmpH,tmpK,tmpS,tmpM,tmpT;
  int poseIndx,py,px,pz,z,pose,width,height,depth;

  // parameters
  pose = fabs(label)-1;  // sample pose
  depth = 6*numPoses;  // fern data depth
  width = img.cols;  // image width
  height = img.rows;  // image height
  poseIndx = pose*6;  // pose depth index

  // pointer to ferns, positive, negative and ratio fern distributions
  unsigned char *fernsPtr = (unsigned char*)(this->ferns.data);
  unsigned char *imgPtr = (unsigned char*)(img.data);
  float* posPtr = this->posHstms.ptr<float>(0);
  float* negPtr = this->negHstms.ptr<float>(0);
  float* ratPtr = this->ratHstms.ptr<float>(0);

  // speedy variables
  tmpF = numFeats*depth;
  tmpT = width*numFerns;
  tmpS = width*numChans;
  tmpW = width - objSize;
  tmpH = height - objSize;

  // random ferns
  for (int fern=0; fern<this->numFerns; fern++){

    // speedy variable
    tmpK = fern*tmpF;

    // fern output
    z = 0;

    // fern features
    for (int feature=0; feature<this->numFeats; feature++){

      // speedy variable
      tmpM = tmpK + feature*depth;

      // point A coordinates
      py = (int)*(fernsPtr + tmpM + (poseIndx+0));
      px = (int)*(fernsPtr + tmpM + (poseIndx+1));
      pz = (int)*(fernsPtr + tmpM + (poseIndx+2));

      // image value A
      valA = *(imgPtr + py*tmpS + px*this->numChans + pz);

      // point B coordinates
      py = (int)*(fernsPtr + tmpM + (poseIndx+3));
      px = (int)*(fernsPtr + tmpM + (poseIndx+4));
      pz = (int)*(fernsPtr + tmpM + (poseIndx+5));

      // image value B
      valB = *(imgPtr + py*tmpS + px*this->numChans + pz);

      // value comparison
      z += (1 << feature) & (0 - ((valA - valB) > 0.001));
    }

    // update distributions
    if (label>0.0)
      *(posPtr + fern*this->numBins*this->numPoses + z*this->numPoses + pose)+= 1.0;

    if (label<0.0)
      *(negPtr + fern*this->numBins*this->numPoses + z*this->numPoses + pose)+= 1.0;

    // update ratio fern distribution
    pos = *(posPtr + fern*this->numBins*this->numPoses + z*this->numPoses + pose);
    neg = *(negPtr + fern*this->numBins*this->numPoses + z*this->numPoses + pose);
    *(ratPtr + fern*this->numBins*this->numPoses + z*this->numPoses + pose) = pos/(pos+neg);

  }
};
// save the classifier
void clsClassifier::fun_save(){

  // variables
  int depth;  // fern data depth
  int poseIndx;  // pose index
  FILE *clfFile;  // file pointer
  float pos,neg,rat;  // fern distribution values
  int xa,ya,za,xb,yb,zb;  // fern values

  // fern data depth
  depth = 6*this->numPoses;

  // open classifier file
  clfFile = fopen(this->filePath,"w");

  // pointer to ferns, positive, negative and ratio fern distributions
  unsigned char *fernsPtr = (unsigned char*)(this->ferns.data);
  float* posPtr = this->posHstms.ptr<float>(0);
  float* negPtr = this->negHstms.ptr<float>(0);
  float* ratPtr = this->ratHstms.ptr<float>(0);

  // messages
  fprintf(clfFile, "Fast Online Learning and Detection of Natural Landmarks for Autonomous Aerial Robots\n\n");
  fprintf(clfFile, "Michael Villamizar\nmvillami.at.iri.upc.edu\n");
  fprintf(clfFile, "Institut de Robotica i Informàtica Industrial (CSIC-UPC)\nBarcelona, Spain\n\n");

  // save parameters
  fprintf(clfFile, "parameters:\n");
  fprintf(clfFile, "objSize: %d\n",this->objSize);  // object size
  fprintf(clfFile, "numFerns: %d\n",this->numFerns);  // num. ferns
  fprintf(clfFile, "numFeats: %d\n",this->numFeats);  // num. binary features per fern
  fprintf(clfFile, "numPoses: %d\n",this->numPoses);  // num. target poses (planar rotations)
  fprintf(clfFile, "numChans: %d\n",this->numChans);  // num. feature channels
  fprintf(clfFile, "threshold: %.2f\n\n",this->threshold);  // classifier threshold

  // save ferns
  fprintf(clfFile, "ferns:\n");
  // random ferns
  for (int fern = 0; fern<this->numFerns; fern++ ){
    for (int feat = 0; feat<this->numFeats; feat++){
      for (int pose = 0; pose<this->numPoses; pose++){

        // pose depth index
        poseIndx = pose*6;

        // get fern values
        ya = *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+0));
        xa = *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+1));
        za = *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+2));
        yb = *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+3));
        xb = *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+4));
        zb = *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+5));

        // save fern values
        fprintf(clfFile, "f: %d %d %d %d %d %d\n",ya,xa,za,yb,xb,zb);

      }
    }
  }

  // save fern distributions
  fprintf(clfFile, "\ndistributions:\n");
  for (int fern = 0; fern<this->numFerns; fern++ ){
    for (int bin = 0; bin<this->numBins; bin++){
      for (int pose = 0; pose<this->numPoses; pose++){

        // value
        pos = *(posPtr + fern*this->numBins*this->numPoses + bin*this->numPoses + pose);	// positive distribution
        neg = *(negPtr + fern*this->numBins*this->numPoses + bin*this->numPoses + pose);	// negative distribution
        rat = *(ratPtr + fern*this->numBins*this->numPoses + bin*this->numPoses + pose);	// ratio distribution

        // save values
        fprintf(clfFile, "d: %.4f %.4f %.4f\n",pos,neg,rat);

      }
    }
  }

  // close file
  fclose(clfFile);

};
// load the classifier
void clsClassifier::fun_load(){

  // variables
  int depth;  // fern data depth
  int poseIndx;  // pose index
  char buffer[128];  // string buffer
  float pos,neg,rat;  // fern distribution values
  FILE *clfFile = NULL;  // file pointer
  int xa,ya,za,xb,yb,zb;  // fern values

  // open file
  if ((clfFile = fopen(this->filePath, "r"))!= NULL){

    // read header
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;

    // read parameters
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fscanf(clfFile, "%s %d", buffer, &this->objSize);
    //cout << "object Size: " << this->objSize << '\n';
    fscanf(clfFile, "%s %d", buffer, &this->numFerns);
    //cout << "num. ferns: " << this->numFerns << '\n';
    fscanf(clfFile, "%s %d", buffer, &this->numFeats);
    //cout << "num. features: " << this->numFeats << '\n';
    fscanf(clfFile, "%s %d", buffer, &this->numPoses);
    //cout << "num. poses: " << this->numPoses << '\n';
    fscanf(clfFile, "%s %d", buffer, &this->numChans);
    //cout << "num. channels: " << this->numChans << '\n';
    fscanf(clfFile, "%s %f", buffer, &this->threshold);
    //cout << "threshold: " << this->threshold << '\n';

    // fern data depth
    depth = 6*this->numPoses;

    // num. histogram bins
    this->numBins = (int)pow(2,this->numFeats);

    // weak classifiers
    this->ferns = cv::Mat(this->numFerns, this->numFeats, CV_8UC(depth));

    // distributions
    this->posHstms = cv::Mat(this->numFerns, this->numBins, CV_32FC(this->numPoses));
    this->negHstms = cv::Mat(this->numFerns, this->numBins, CV_32FC(this->numPoses));
    this->ratHstms = cv::Mat(this->numFerns, this->numBins, CV_32FC(this->numPoses));

    // pointer to ferns, positive, negative and ratio fern distributions
    unsigned char *fernsPtr = (unsigned char*)(this->ferns.data);
    float* posPtr = this->posHstms.ptr<float>(0);
    float* negPtr = this->negHstms.ptr<float>(0);
    float* ratPtr = this->ratHstms.ptr<float>(0);

    // read ferns
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    for (int fern = 0; fern<this->numFerns; fern++ ){
      for (int feat = 0; feat<this->numFeats; feat++){
        for (int pose = 0; pose<this->numPoses; pose++){

          // pose depth index
          poseIndx = pose*6;

          // get fern values
          fscanf(clfFile, "%s %d %d %d %d %d %d",buffer,&ya,&xa,&za,&yb,&xb,&zb);
          //printf("%d %d %d %d %d %d \n",ya,xa,za,yb,xb,zb);

          // set fern values
          *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+0)) = ya;
          *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+1)) = xa;
          *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+2)) = za;
          *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+3)) = yb;
          *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+4)) = xb;
          *(fernsPtr + fern*this->numFeats*depth + feat*depth + (poseIndx+5)) = zb;

        }
      }
    }

    // read fern distributions
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    fgets(buffer, 128, clfFile);
    //cout << buffer;
    for (int fern = 0; fern<this->numFerns; fern++ ){
      for (int bin = 0; bin<this->numBins; bin++){
        for (int pose = 0; pose<this->numPoses; pose++){

         // get fern values
         fscanf(clfFile, "%s %f %f %f",buffer,&pos,&neg,&rat);
         //printf("%f %f %f \n",pos,neg,rat);

         // value
         *(posPtr + fern*this->numBins*this->numPoses + bin*this->numPoses + pose) = pos;	// positive distribution
         *(negPtr + fern*this->numBins*this->numPoses + bin*this->numPoses + pose) = neg;	// negative distribution
         *(ratPtr + fern*this->numBins*this->numPoses + bin*this->numPoses + pose) = rat;	// ratio distribution

        }
      }
    }

    // close file
    fclose(clfFile);

  }
  else {
    cout << "ERROR : not input detector file" << endl;
    exit(0);
  }
};
// print classifier parameters
void clsClassifier::fun_print(){
  cout << "\n*********************************************************" << endl;
  cout << "* Classifier Parameters : " << endl;
  cout << "* num. Ferns -> " << this->numFerns << endl;
  cout << "* num. Features -> " << this->numFeats << endl;
  cout << "* num. Poses -> " << this->numPoses << endl;
  cout << "* object size ->  " << this->objSize << "x" << this->objSize << endl;
  cout << "* threshold -> " << this->threshold << endl;
  cout << "*********************************************************\n" << endl;
};



// clsDetection
// constructor
clsDetection::clsDetection(){
  // initialize default values
  fun_initialize();
};
// destructor
clsDetection::~clsDetection(){
};
// initialize with default values
void clsDetection::fun_initialize(){
  // default parameters
  this->x1 = 0;  // detection location
  this->y1 = 0;
  this->x2 = 0;
  this->y2 = 0;
  this->pose = 0;  // detection pose
  this->score = 0.0;  // detection score
  this->scale = 0;  // detection scale
};
// set values
void clsDetection::fun_set_values(int xa, int ya, int xb, int yb, float score, int pose, int scale){
  this->x1 = xa;  // detection location
  this->y1 = ya;
  this->x2 = xb;
  this->y2 = yb;
  this->pose = pose;   // detection pose
  this->score = score;  // detection score
  this->scale = scale;  // detection scale
};
// get values
void clsDetection::fun_get_values (int &xa, int &ya, int &xb, int &yb, float &score, int &pose, int &scale){
  xa = this->x1;  // detection coordinates
  ya = this->y1;
  xb = this->x2;
  yb = this->y2;
  pose = this->pose;  // detection pose
  scale = this->scale;  // detection scale
  score = this->score;  // detection score
};



// clsDetectionSet
// constructor
clsDetectionSet::clsDetectionSet(){
  // initialize
  fun_initialize();
};
// Destructor
clsDetectionSet::~clsDetectionSet(){
  // relase memory
  fun_release();
};
// initialize
void clsDetectionSet::fun_initialize(){
  // default values
  this->numDets = 0;  // number of detections
  this->numMaxDets = 500;  // number muximum of detections
  // array of detections
  this->det = new clsDetection[this->numMaxDets];
};
// release
void clsDetectionSet::fun_release(){
  delete[]this->det;
};
// get the number of detections
int clsDetectionSet::fun_get_num_detections(){
  return this->numDets;
};
// set the number of detections
void clsDetectionSet::fun_set_num_detections(int value){
  this->numDets = value;
};
// get the number muximum of detections
int clsDetectionSet::fun_get_num_max_detections(){
  return this->numMaxDets;
};
// get detection
clsDetection* clsDetectionSet::fun_get_detection(int index){
  clsDetection* detection = &this->det[index];
  return detection;
};
// set a new detection
void clsDetectionSet::fun_set_detection(clsDetection* detection, int index){

// variable
float score;
int x1,y1,x2,y2,pose,scale;

// get input detection values
detection->fun_get_values(x1, y1, x2, y2, score, pose, scale);

// set detection values
this->det[index].fun_set_values(x1, y1, x2, y2, score, pose, scale);
};
// scaling
void clsDetectionSet::fun_scaling(float scaleFactor){

  // variables
  float score;
  int y1,x1,y2,x2,pose,scale;

  // scaling detection coordinates
  for (int iter=0; iter<this->numDets; iter++ ){

  // scaling spatial coordinates
  this->det[iter].fun_get_values(x1, y1, x2, y2, score, pose, scale);
  x1 = (int)round(x1*scaleFactor);
  y1 = (int)round(y1*scaleFactor);
  x2 = (int)round(x2*scaleFactor);
  y2 = (int)round(y2*scaleFactor);
  this->det[iter].fun_set_values(x1, y1, x2, y2, score, pose, scale);
  }
};
// add detections
void clsDetectionSet::fun_add_detections(clsDetectionSet* detections){

  // variables
  int numDetections;

  // number of detections 
  numDetections = detections->fun_get_num_detections();

  // check
  if (this->numDets + numDetections >= this->numMaxDets){
    printf("!! ERROR: num. max. detections is %d \n",this->numMaxDets);
  }
  else{
    // add detections 
    for (int iter=0; iter<numDetections; iter++){
      this->det[numDets+iter] = detections->det[iter];
    }
    // updates the number of detections 
    this->numDets += numDetections;
  }
};
// maxima detection
void clsDetectionSet::fun_get_max_detection(clsDetection* maxDetection){

  // variables
  int x1,y1,x2,y2,pose,scale;
  float score,maxScore = 0;

  // detections
  for (int iter=0; iter<this->numDets; iter++){

    // get detection values
    this->det[iter].fun_get_values(x1, y1, x2, y2, score, pose, scale);

    // best score
    if (score>maxScore){

      // set the max. detection
      maxDetection->fun_set_values(x1, y1, x2, y2, score, pose, scale);

      // update max. score
      maxScore = score;

    }
  }
};
// remove detections
void clsDetectionSet::fun_remove_detections(clsDetection* detection){

  // parameters
  float THRESHOLD = 0.01;  // overlapping rate threshold

  // variables
  int counter = 0;  // counter
  int u1,v1,u2,v2;  // overlapping coordinates
  clsDetection empty;  // empty detection
  float score,refScore;  // detection scores
  int x1,y1,x2,y2,pose,scale;  // detection coordinates
  float width,height,area,rarea;  // overlapping variables
  int rx1,ry1,rx2,ry2,refPose,refScale;  // reference detection coordinates

  // number of initial detections
  this->numDets = fun_get_num_detections();

  // get reference detection values
  detection->fun_get_values(rx1, ry1, rx2, ry2, refScore, refPose, refScale);

  // reference area
  rarea = (ry2-ry1)*(rx2-rx1);

  // check each detection
  for (int iter=0; iter<this->numDets; iter++){

    // get detection values
    this->det[iter].fun_get_values(x1, y1, x2, y2, score, pose, scale);

    // overlapping values
    u1 = max(rx1,x1);
    v1 = max(ry1,y1);
    u2 = min(rx2,x2);
    v2 = min(ry2,y2);
    width  = u2-u1;
    height = v2-v1;
    area = width*height;

    // remove detections
    if (width<=0 || height<=0 || (area/rarea)<THRESHOLD){
      // no intersection
      this->det[counter].fun_set_values(x1, y1, x2, y2, score, pose, scale);
      counter++;
    }
  }

  // delete detections
  for (int iter=counter; iter<this->numDets; iter++){
    this->det[iter] = empty;
  }

  // update the number of detections
  this->fun_set_num_detections(counter);

};
// non-maxima suppresion
void clsDetectionSet::fun_non_maxima_supression(){

  // variables
  int counter = 0;	// detection counter

  // temporal detection set
  clsDetectionSet* detSet = new clsDetectionSet;

  // number of initial detections
  this->numDets = fun_get_num_detections();

  // check detections
  if (this->numDets>0){

    // remove detections
    while (this->fun_get_num_detections()!=0){

      // max. detection
      clsDetection* maxDet = new clsDetection;

      // get max. detection
      this->fun_get_max_detection(maxDet);

      // get non-intersection detections
      this->fun_remove_detections(maxDet);

      // add detection: max. detection
      detSet->fun_set_detection(maxDet, counter);

      // update counter
      counter++;

      // set the number of detections
      detSet->fun_set_num_detections(counter);

      // release
      delete maxDet;
    }
  }

  // add max. detections: non-maxima suppression
  this->fun_add_detections(detSet);

  // release
  delete detSet;
};



// clsII
// constructor
clsII::clsII(){
  // default values
  this->width = 0;  // integral image width
  this->height = 0;  // integral image height
  this->imgWidth = 0;  // image width
  this->imgHeight = 0;  // image height
  this->numChannels = 3;  // number of image feature channels
};
// destructor
clsII::~clsII(){
};
// get integral image
cv::Mat clsII::fun_get_image() {
  return this->img;
};
// get image size
void clsII::fun_get_image_size(int& sx, int& sy) {
  sx = this->imgWidth;
  sy = this->imgHeight;
};
// compute image
void clsII::fun_compute_image(int size){

  // set values
  this->cellSize = size;  // cell size
  this->imgWidth = (int)ceil((float)this->width/this->cellSize);  // image width
  this->imgHeight = (int)ceil((float)this->height/this->cellSize);  // image height

  // variables
  int w = this->width;
  int h = this->height;
  int n = this->numChannels;
  int s = this->cellSize;
  int t,b,l,r;
  double tl,bl,tr,br,value;

  // create image
  this->img = cv::Mat(cvSize(this->imgWidth,this->imgHeight), CV_8UC(this->numChannels), cv::Scalar::all(0));

  // pointer to image and integral image data
  double* IIPtr = this->II.ptr<double>(0);
  unsigned char *imgPtr = (unsigned char*)(this->img.data);

  // scanning
  for (int y=0; y<this->imgHeight-1; y++) {

    // local coordinates
    t = y*s;  // top
    b = t + s;  // bottom

    for (int x=0; x<this->imgWidth-1; x++) {

      // local coordinates
      l = x*s;  // left
      r = l + s;  // right

      // check
      if (t<0 || b>=h || l<0 || r>=w)
        printf("Warning: incorrect corner coordinates -> top: %d left: %d bottom: %d right: %d img. height %d img. width%d \n",t,l,b,r,h,w);

      for (int c=0; c<this->numChannels; c++){

        // integral image values
        tl = *(IIPtr + t*w*n + l*n + c);  // top-left corner
        bl = *(IIPtr + b*w*n + l*n + c);  // bottom-left corner
        tr = *(IIPtr + t*w*n + r*n + c);  // top-right corner
        br = *(IIPtr + b*w*n + r*n + c);  // bottom-right corner

        // check
        if (tl<0 || bl<0 || tr<0 || br<0)
          printf("Warning: incorrect corner values -> top-left: %.3f bottom-left: %.3f top-right: %.3f bottom:right: %.3f \n",tl,bl,tr,br);

        // image value
        value = br + tl - tr - bl;
        value = (double)value/(s*s);

        // check (2 -> small error)
        if (value<0 || value>255+2)
          printf("Warning: incorrect image value -> %.3f \n",value);

        // image
        *(img.data + y*this->imgWidth*n + x*n + c) = (int) value;

      }
    }
  }
};
// compute integral image
void clsII::fun_integral_image(cv::Mat image){

  // set II size
  this->width = image.cols;
  this->height = image.rows;
  this->numChannels = image.channels();

  // create II
  this->II = cv::Mat(this->height, this->width, CV_64FC(this->numChannels), cv::Scalar::all(0));

  // variables
  int w = this->width;
  int h = this->height;
  int n = this->numChannels;
  double imgValue,values;

  // pointers to image and integral image data
  double* IIPtr = II.ptr<double>(0);
  unsigned char *imgPtr = (unsigned char*)(image.data);

  // construction
  for (int y=0; y<this->height; y++){
    for (int x=0; x<this->width; x++){
      for (int c=0; c<this->numChannels; c++){

        // image pixel value
        imgValue = *(imgPtr + y*w*n + x*n + c);

        // integral image values
        if (x>0){
          if (y>0){
            values = *(IIPtr + y*w*n + (x-1)*n + c) + *(IIPtr + (y-1)*w*n + x*n + c) - *(IIPtr + (y-1)*w*n + (x-1)*n + c);
          }
          else {
            values = *(IIPtr + y*w*n + (x-1)*n + c);
          }
        }
        else {
          if (y>0){
            values = *(IIPtr + (y-1)*w*n + x*n + c);
          }
          else {
            values = 0;
          }
        }

        // compute current pixel value in II
        *(IIPtr + y*w*n + x*n + c) = values + imgValue;

        // check
        if (values + imgValue > (y+1)*(x+1)*255)
          printf("Warning incorrect integral image value -> %.2f\n",values + imgValue);

      }
    }
  }
};
// compute integral image
void clsII::fun_release_image(){
  // release image
  this->img.release();
};



// clsFrameData
// constructor
clsFrameData::clsFrameData(){
  // variables
  list<int> poses;  // detection poses
  list<CvRect> boxes;  // detection bounding boxes
  list<int> scales;  // detection scales
};
// destructor
  clsFrameData::~clsFrameData(){
};


// clsDetROI
// constructor
clsDetROI::clsDetROI(){
  // variables
  int x = 0;  // location
  int y = 0;
  int width = 0;  // width
  int scale = 0;  // scale
  int height = 0;  // height
  bool active = false;  // ROI status flag
};
// destructor
  clsDetROI::~clsDetROI(){
};
// clear ROI values
void clsDetROI::fun_clear(){
  // reset ROI values
  this->x = 0;
  this->y = 0;
  this->width = 0;
  this->scale = 0;
  this->height = 0;
  this->active = false;
};
// get the ROI scale
int clsDetROI::fun_get_scale(){
  return this->scale;
};
// get the ROI box
void clsDetROI::fun_get_box(int& x, int& y, int& width, int& height){
  // return the box
  x = this->x;
  y = this->y;
  width = this->width;
  height = this->height;
};
// set the ROI box
void clsDetROI::fun_set_box(int x, int y, int width, int height){
  // set values
  this->x = x;
  this->y = y;
  this->width = width;
  this->height = height;
};
// get the ROI status {active,desactive}
bool clsDetROI::fun_get_status(){
  return this->active;
};
// scale ROI box
void clsDetROI::fun_scale_box(float factor){
  // scaling bounding box coordinates
  this->x = (int)round(this->x/factor);
  this->y = (int)round(this->y/factor);
  this->width = (int)round(this->width/factor);
  this->height = (int)round(this->height/factor);
};
// copy ROI values
void clsDetROI::fun_copy_values(clsDetROI ROI){
  // variables
  int tx,ty,tw,th;

  // get box values
  ROI.fun_get_box(tx,ty,tw,th);

  // copy values
  this->active = ROI.fun_get_status();
  this->scale = ROI.fun_get_scale();
  this->fun_set_box(tx,ty,tw,th);

};
// set ROI values
void clsDetROI::fun_set_values(CvRect box, int scale){
  // set values
  this->x = box.x;
  this->y = box.y;
  this->width = box.width;
  this->height = box.height;
  this->scale = scale;
  this->active = true;
};











