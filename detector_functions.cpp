#include <string>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include "detector_classes.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

#define PI 3.14159265
using namespace std;

// save image
void fun_save_image(cv::Mat img, long int number){

  // num. max. images
  int numMaxImgs = 10000;

  // variables
  char imgName[50];

  // path and image name
  if (number<10000)
    sprintf(imgName, "../images/image_%ld.jpg",number);
  if (number<1000)
    sprintf(imgName, "../images/image_0%ld.jpg",number);
  if (number<100)
    sprintf(imgName, "../images/image_00%ld.jpg",number);
  if (number<10)
    sprintf(imgName, "../images/image_000%ld.jpg",number);

  // save image
  if (number<numMaxImgs)
    cv::imwrite(imgName, img);

};

// draw message
void fun_draw_message(clsParameters* prms, cv::Mat img, char* text, CvPoint location, CvScalar textColor){

  // text font
  float font = prms->fun_get_font();

  // message
  cv::putText(img, text, location, cv::FONT_HERSHEY_PLAIN, font, textColor, 4);

};

// show frame
void fun_show_frame(clsParameters* prms, cv::Mat img, int contFrame, double FPS){

  // text font
  float font = prms->fun_get_font();

  // rectangle color and thickness
  CvScalar textColor = prms->fun_get_color();

  // rectangle coordinates
  CvPoint xy1 = cvPoint(2,30);
  CvPoint xy2 = cvPoint(250,30);

  // text variables
  char text1[50];
  char text2[50];

  // messages
  sprintf(text1, "#%i",contFrame);
  sprintf(text2, "FPS: %.1f",FPS);
  cv::putText(img, text1, xy1, cv::FONT_HERSHEY_PLAIN, font, textColor, 4);
  cv::putText(img, text2, xy2, cv::FONT_HERSHEY_PLAIN, font, textColor, 4);

};

// show image patch
void fun_show_image_patch(clsParameters* prms, cv::Mat img, clsDetectionSet* detSet){

  // patch size
  int patchSize = prms->fun_get_patch_size();

  // max. detection
  clsDetection* maxDet = new clsDetection;

  // visualization area (top-right corner)
  CvRect area = cvRect(img.cols-patchSize, 1, patchSize, patchSize);

  // maxima detection
  detSet->fun_get_max_detection(maxDet);

  // detection variables
  float score;  // detection score
  int x1,y1,x2,y2,scale,pose;  // detection location

  // detection values
  maxDet->fun_get_values(x1, y1, x2, y2, score, pose, scale);

  // bounding box
  CvRect rec = cvRect(x1, y1, x2-x1, y2-y1);

  // check
  if (x1!=0 && x2!=0 && y1!=0 && y2!=0){

    // patch
    cv::Mat patch = cv::Mat(cvSize(patchSize,patchSize), CV_8UC3);
    cv::resize(img(rec), patch, cvSize(patchSize,patchSize));

    // add patch into image
    addWeighted(img(area), 0.1, patch, 0.9, 0.0, img(area));
  }

  // release
  delete maxDet;
};

// image equalization
void fun_image_equalization(cv::Mat img){

  // temporal image
  cv::Mat img_space = img;
  cv::resize(img_space, img_space, cvSize(img.cols, img.rows));

  // convert color space: YCrCb or Lab
  cvtColor(img,img_space,CV_BGR2Lab);

  // split image into color channels
  std::vector<cv::Mat> img_channels;
  cv::split(img_space, img_channels);

  // equalize histograms
  cv::equalizeHist(img_channels[0], img_channels[0]);  // equalize channel 1

  // merge channels
  cv::merge(img_channels, img_space);

  // convert color space: YCrCb or Lab
  cvtColor(img_space, img, CV_Lab2BGR);

};

// draw detection mode
void fun_draw_detection_mode(cv::Mat img, bool status){

  // variables
  CvPoint location;
  int rad1,rad2,thk;
  CvScalar color,black;

  // parameters
  thk = 14; // thickness
  rad1 = 12;  // outer radius
  rad2 = 6;  // inner radius
  black = cvScalar(0,0,0);  // black color
  location = cvPoint(img.cols-30, img.rows-30);  // location

  // check detection ROI status: false-> detection - true-> tracking
  if (status==false){
    // detection -detection over the entire image-
    color = cvScalar(0,0,255);
  }
  else {
    // tracking -detection over search region-
    color = cvScalar(0,255,0);
  }

  // draw circles
  cv::circle(img, location, rad1, black, thk, 8, 0);
  cv::circle(img, location, rad2, color, thk, 8, 0);

};

// draw detection score
void fun_draw_detection_score(clsParameters* prms, cv::Mat img, clsDetectionSet* detSet){

  // variables
  float score;
  char text[50];
  CvScalar textColor;
  int x1,y1,x2,y2,pose,scale;
  CvPoint location = cvPoint(img.cols-160, 30);

  // text color
  textColor = prms->fun_get_color();

  // max. detection
  clsDetection* maxDet = new clsDetection;

  // maxima detection
  detSet->fun_get_max_detection(maxDet);

  // detection values
  maxDet->fun_get_values(x1, y1, x2, y2, score, pose, scale);

  // detection score
  sprintf(text, "%.2f",score);

  // message
  fun_draw_message(prms, img, text, location, textColor);

  // release
  delete maxDet;
};

// draw detection threshold
void fun_draw_detection_threshold(clsParameters* prms, cv::Mat img, float threshold){

  // variables
  char text[50];
  CvScalar black;
  float textFont;
  CvPoint location;

  // parameters
  black = cvScalar(0,0,0);  // black color
  textFont = prms->fun_get_font();  // font size
  location = cvPoint(img.cols-80, 30);  // text location

  // detection threshold
  sprintf(text, "%.2f",threshold);

  // message
  fun_draw_message(prms, img, text, location, black);

};

// draw frame data
void fun_draw_frame_data(clsParameters* prms, cv::Mat img, clsFrameData &frmData){

  // variables
  float ang;
  CvRect box;
  CvPoint center,pt1,pt2;
  CvScalar detColor;
  int numBoxes,detThick;
  int radius,pose,numPoses;

  // parameters
  numBoxes = frmData.boxes.size();  // num. bounding boxes -detections-
  detColor = prms->fun_get_color();  // detection color
  detThick = prms->fun_get_rectangle_thickness();  // detection rectangle -box- thickness
  numPoses = prms->fun_get_num_poses();  // num. object poses

  // boxes
  for (int iter=0; iter<numBoxes; iter++){

    // detection properties
    box = frmData.boxes.back();  // bounding box
    pose = frmData.poses.back();  // detection pose

    // circle properties
    ang = (pose-1)*2*PI/numPoses;  // orientation angle
    radius = floor(box.width/2);  // radius
    center = cvPoint(box.x + radius, box.y + radius);  // center

    // orientation line
    pt1 = center;
    pt2 = cvPoint(center.x + radius*sin(ang), center.y + radius*cos(ang));

    // draw circle
    cv::circle(img, center, radius, detColor, detThick, 8, 0);

    // draw orientation line
    cv::line(img, pt1, pt2, detColor, detThick, 8, 0);

  }
};

// frame data -detections-
void fun_frame_data(clsDetectionSet* detSet, cv::Mat labels, clsFrameData &frmData){

  // variables
  CvRect box;
  int numDets;
  float label,score;
  clsDetection* det;
  int x1,y1,x2,y2,pose,scale;

  // pointer to labels
  float* labPtr = labels.ptr<float>(0);

  // num. detections
  numDets = detSet->fun_get_num_detections();

  // detections
  for (int iter=0; iter<numDets; iter++){

    // current detection label
    label = *(labPtr + iter);

    // true positive
    if (label==1.0){

      // get detection
      det = detSet->fun_get_detection(iter);

      // detection values
      det->fun_get_values(x1, y1, x2, y2, score, pose, scale);

      // bounding box
      box = cvRect(x1, y1 ,x2-x1, y2-y1);

      // add detection
      frmData.boxes.push_back(box);
      frmData.poses.push_back(pose);
      frmData.scales.push_back(scale);

    }
  }
};

// detection labels
cv::Mat fun_detection_labels(clsParameters* prms, clsDetectionSet* detSet){

  // detection and assistance thresholds
  float detThr = prms->fun_get_threshold();
  float omega = prms->fun_get_assistance_threshold();

  // number of detections
  int numDets = detSet->fun_get_num_detections();

  // create detection labels
  cv::Mat labels = cv::Mat(numDets, 1, CV_32FC1, cv::Scalar::all(0));

  // pointer to labels
  float* labPtr = labels.ptr<float>(0);

  // detection variables
  float score;  // detection score
  int xa,ya,xb,yb,scale,pose;  // detection location

  // detections
  for (int iter=0; iter<numDets; iter++){

    // current detection
    clsDetection* det = detSet->fun_get_detection(iter);

    // detection values
    det->fun_get_values(xa, ya, xb, yb, score, scale, pose);

    // positive detection
    if (score>detThr + 0.5*omega){*(labPtr + iter) = 1.0;}

    // negative detection
    if (score<detThr - 0.5*omega){*(labPtr + iter) = -1.0;}

    // uncertainty classification region (defined by omega around the classifier threshold)
    if ((score >= detThr - 0.5*omega) && (score <= detThr + 0.5*omega)){*(labPtr + iter) = 0.5;}

  }

  // return detection labels
  return labels;
};

// test the classifier
void fun_test_classifier(clsClassifier* clfr, cv::Mat img, clsDetectionSet* detSet, CvRect area, int scale){

  // variables
  int detCont = 0;
  cv::Mat ferns, ratHstms;
  int minY,minX,maxY,maxX;
  float valA,valB,maxScore;
  int tmpF,tmpW,tmpH,tmpK,tmpS,tmpM,tmpT,depth;
  int py,px,pz,z,objSize,numMaxDets,numPoses,maxPose;
  int numFeats,numBins,numFerns,width,height,numChans;

  // parameters
  int OFFSET = 5;  // search area offset
  bool SPEED = true;  // naive cascade
  int MINFERNS = 20;  // num. min. ferns
  float THRESHOLD = 0.40; // default detector threshold
  float MINTHRESHOLD = 0.40;  // min. cascade threshold

  // parameters
  ferns = clfr->fun_get_ferns();  // pointer to classifier data (ferns data)
  width = img.cols;  // image width
  height = img.rows;  // image height
  objSize = clfr->fun_get_object_size();  // object size
  numFerns = clfr->fun_get_num_ferns();  // num. random ferns
  ratHstms = clfr->fun_get_ratHstms();  // classifier distributions
  numBins = ratHstms.cols;  // num. histogram bins
  numChans = clfr->fun_get_num_channels();  // num. image feature channels
  numFeats = clfr->fun_get_num_features();  // number of features per fern
  numPoses = clfr->fun_get_num_poses();  // num. classifier poses (object poses)
  depth = 6*numPoses;  // fern data depth
  numMaxDets = detSet->fun_get_num_max_detections();  // num. max. detections

  // pointer to ferns data, image and ratio fern distributions
  unsigned char *fernsPtr = (unsigned char*)(ferns.data);
  unsigned char *imgPtr = (unsigned char*)(img.data);
  float* ratPtr = ratHstms.ptr<float>(0);

  // detection scores
  float scores[numPoses];

  // speedy variables
  tmpF = numFeats*depth;
  tmpT = width*numFerns;
  tmpS = width*numChans;
  tmpW = width - objSize;
  tmpH = height - objSize;

  // default scanning values
  minY = 0;
  minX = 0;
  maxY = height-objSize;
  maxX = width-objSize;

  // check area is active (ROI)
  if (area.width!=0 && area.height!=0){

    // new limits to the input area + offset
    minY = area.y - OFFSET;
    minX = area.x - OFFSET;
    maxY = area.y + area.height + OFFSET;
    maxX = area.x + area.width + OFFSET;

    // check limits
    if (minY<0) minY = 0;
    if (minX<0) minX = 0;
    if (maxY>height-objSize) maxY = height-objSize;
    if (maxX>width-objSize) maxX = width-objSize;
  }

  // scanning
  for (int y=minY; y<maxY; y++){
    for (int x=minX; x<maxX; x++){

      // initial values
      maxPose = 0;  // pose for max. detection score
      maxScore = 0.0;  // max. detection score

      // set detection scores
      for (int pose=0; pose<numPoses; pose++){
        scores[pose] = 0.0;
      }

      // test the ferns
      for (int fern=0; fern<numFerns; fern++){

        // speedy variable
        tmpK = fern*tmpF;

        // fern output
        z = 0;

        // fern features
        for (int feat=0; feat<numFeats; feat++){

          // speedy variable
          tmpM = tmpK + feat*depth;

          // point A coordinates
          py = y + (int)*(fernsPtr + tmpM + 0);
          px = x + (int)*(fernsPtr + tmpM + 1);
          pz =     (int)*(fernsPtr + tmpM + 2);

          // image value A
          valA = *(imgPtr + py*tmpS + px*numChans + pz);

          // point B coordinates
          py = y + (int)*(fernsPtr + tmpM + 3);
          px = x + (int)*(fernsPtr + tmpM + 4);
          pz =     (int)*(fernsPtr + tmpM + 5);

          // image value B
          valB = *(imgPtr + py*tmpS + px*numChans + pz);

          // value comparison
          z += (1 << feat) & (0 - ((valA - valB) > 0.001));

        }

        // update detection scores and max. score/pose
        for (int pose=0; pose<numPoses; pose++){

          // pose scores
          scores[pose]+= *(ratPtr + fern*numBins*numPoses + z*numPoses + pose);

          // best score and pose
          if (scores[pose]>maxScore){
            maxPose = pose;  // current pose
            maxScore = scores[pose];  // max. score
          }
        }

        // a naive cascade to speed up detection
        if (SPEED==true && fern>MINFERNS && (maxScore/fern)<MINTHRESHOLD) break;

      }

      // normalize score
      maxScore = maxScore/numFerns;

      // check amount of detections
      if (detCont>numMaxDets)
      printf("!! Warning: there are many hypotheses: low threshold\n");

      // save detections
      if(maxScore>THRESHOLD && detCont<numMaxDets){

      // temporal detection
      clsDetection* det = new clsDetection;

      // set detection values
      det->fun_set_values(x+1, y+1, x+objSize, y+objSize, maxScore, maxPose+1, scale);

      // add detection to detection set
      detSet->fun_set_detection(det, detCont);

      // increment the number of detections
      detCont++;

      // save the number of detections
      detSet->fun_set_num_detections(detCont);

      // release
      delete det;
      }
    }
  }
};

// object detection using input search area
void fun_detect(clsParameters* prms, clsClassifier* clfr, cv::Mat img, clsDetROI ROI, clsDetectionSet* detSet){

  // variables
  clsII II;
  CvRect area;
  int OFFSET = 3;
  int x,y,width,height;
  int numMaxDets,objSize;
  int scale,minScale,maxScale;
  int minCell,maxCell,numFerns;

  // parameters
  minCell = prms->fun_get_min_cell_size();  // min. cell size
  maxCell = prms->fun_get_max_cell_size();  // min. cell size
  numFerns = clfr->fun_get_num_ferns();  // num. random ferns

  // compute integral image
  II.fun_integral_image(img);

  // maximum number of output detections
  numMaxDets = detSet->fun_get_num_max_detections();

  // defatult scale values
  minScale = minCell;
  maxScale = maxCell;

  // check ROI: if true the space search is reduced
  if (ROI.fun_get_status()==true){

    // ROI scale
    scale = ROI.fun_get_scale();

    // new scale values
    minScale = scale - OFFSET;
    maxScale = scale + OFFSET;

    // check scale limits
    if (minScale<minCell)
      minScale = minCell;
    if (maxScale>maxCell)
      maxScale = maxCell;
  }

  // image levels -cell size-
  for (int iterScale=minScale; iterScale<maxScale; iterScale++){

    // temporal detections
    clsDetectionSet* dets = new clsDetectionSet;

    // compute image
    II.fun_compute_image(iterScale);
    II.fun_get_image_size(width, height);

    // scaled detection ROI
    clsDetROI scaROI;
    scaROI.fun_copy_values(ROI);
    scaROI.fun_scale_box(iterScale);
    scaROI.fun_get_box(x,y,width,height);

    // search area
    area = cvRect(x,y,width,height);

    // test the classifier
    fun_test_classifier(clfr, II.fun_get_image(), dets, area, iterScale);

    // non maxima supression
    dets->fun_non_maxima_supression();

    // scaling detection coordinates
    dets->fun_scaling(iterScale);

    // adding detections
    detSet->fun_add_detections(dets);

    // clear memory
    II.fun_release_image();

    // release
    delete dets;
  }

  // non maxima supression
  detSet->fun_non_maxima_supression();
};

// update the classifier using positive smaples
void fun_update_positive_samples(clsClassifier* clfr, cv::Mat img, CvRect box, int pose, int numSamples, float shift){

  // variables
  CvRect newBox;
  int xa,ya,xb,yb;
  int MINSIZE,objSize,numFerns,numPoses;

  // parameters
  MINSIZE = 5;  // min. image size
  objSize = clfr->fun_get_object_size();  // object size
  numFerns = clfr->fun_get_num_ferns();  // num. random ferns
  numPoses = clfr->fun_get_num_poses();  // num. classifier poses

  // positive samples
  for (int iter=0; iter<numSamples; iter++){

    // new box coordinates
    ya = box.y + round(shift*box.height*((float)rand()/RAND_MAX - 0.5));
    xa = box.x + round(shift*box.width*((float)rand()/RAND_MAX  - 0.5));
    yb = box.y + box.height + round(shift*box.height*((float)rand()/RAND_MAX - 0.5));
    xb = box.x + box.width + round(shift*box.width*((float)rand()/RAND_MAX  - 0.5));

    // check
    if ((xb-xa)>MINSIZE && (yb-ya)>MINSIZE){

        // check limits
      if (xa<0){xa = 0;}
      if (ya<0){ya = 0;}
      if (xb>=img.cols){xb = img.cols-1;}
      if (yb>=img.rows){yb = img.rows-1;}

      // new bounding box
      newBox = cvRect(xa, ya, xb-xa, yb-ya);

      // image patch using ROI
      cv::Mat patch = cv::Mat(cvSize(objSize,objSize), CV_8UC3);
      cv::resize(img(newBox), patch, cvSize(objSize,objSize));

      // update classifier
      clfr->fun_update(patch, pose);

    }
  }
};

//update the classifier using negative samples
void fun_update_negative_samples(clsClassifier* clfr, cv::Mat img, CvRect box, int pose, int numSamples, float shift){

  // variables
  CvRect newBox;
  int xa,ya,xb,yb;
  int xi,xj,yi,yj;
  int MINSIZE,objSize,numFerns,numPoses;

  // parameters
  MINSIZE = 5;  // min. image size
  objSize = clfr->fun_get_object_size();  // object size
  numFerns = clfr->fun_get_num_ferns();  // num. random ferns
  numPoses = clfr->fun_get_num_poses();  // num. classifier poses

  // negative samples
  for (int iter=0; iter<numSamples; iter++){

    // new box coordinates (random location)
    xi = round(img.cols*((float)rand()/RAND_MAX));
    xj = round(img.cols*((float)rand()/RAND_MAX));
    yi = round(img.rows*((float)rand()/RAND_MAX));
    yj = round(img.rows*((float)rand()/RAND_MAX));
    xa = min(xi, xj);
    xb = max(xi, xj);
    ya = min(yi, yj);
    yb = max(yi, yj);

    // check
    if ((xb-xa)>MINSIZE && (yb-ya)>MINSIZE){

      // check limits
      if (xa<0){xa = 0;}
      if (ya<0){ya = 0;}
      if (xb>=img.cols){xb = img.cols-1;}
      if (yb>=img.rows){yb = img.rows-1;}

      // new bounding box
      newBox = cvRect(xa, ya, xb-xa, yb-ya);

      // image patch using ROI
      cv::Mat patch = cv::Mat(cvSize(objSize,objSize), CV_8UC3);
      cv::resize(img(newBox), patch, cvSize(objSize,objSize));

      // update classifier
      clfr->fun_update(patch, -1*pose);

    }
  }
};

// update classifier
void fun_update_classifier(clsParameters* prms, clsClassifier* clfr, cv::Mat img, clsDetectionSet* detSet, cv::Mat labels){

  // parameters
  int objSize = clfr->fun_get_object_size();  // object size
  float shift  = prms->fun_get_image_shift();  // image shift factor
  int numSamples = prms->fun_get_num_new_samples();  // number of new -extracted- samples

  // variables
  float score;  // detection score
  int height,width;  // object size
  int x1,y1,x2,y2,scale,pose; // detection location

  // positive and negative values
  float posValue = 1.0;
  float negValue = -1.0;

  // number of detections
  int numDets = detSet->fun_get_num_detections();

  // check
  if ((numDets!=0) && (numSamples>0)){

    // pointer to labels
    float* labPtr = labels.ptr<float>(0);

    // detections
    for (int iter=0; iter<numDets; iter++){

      // current detection
      clsDetection* det = detSet->fun_get_detection(iter);

      // detection values
      det->fun_get_values(x1, y1, x2, y2, score, scale, pose);

      // new bounding box
      CvRect box = cvRect(x1, y1, x2-x1, y2-y1);

      // image patch using ROI
      cv::Mat patch = cv::Mat(cvSize(objSize,objSize), CV_8UC3);
      cv::resize(img(box), patch, cvSize(objSize,objSize));

      // sample label
      float label = (float)*(labPtr + iter);

      // uncertainty classification region
      if (label==0.5){
        // the classifier is not update in uncertain cases
      }

      // update positive sample
      if (label==1.0){
        // update positive sample
        clfr->fun_update(patch, posValue);
        // update using a set of positive samples
        fun_update_positive_samples(clfr, img, box, pose, numSamples, shift);
      }

      // update negative sample
      if (label==-1.0){
        // update negative sample
        clfr->fun_update(patch, negValue);
        // update using a set of negative samples
        fun_update_negative_samples(clfr, img, box, pose, numSamples, shift);
      }

    }
  }
};

// train the classifier
void fun_train_classifier(clsParameters* prms, clsClassifier* clfr, cv::Mat img, CvRect box){

  // variables
  float ratio,detThr,imgShift;
  int height,width,numPoses,objSize;
  int numSamples,numFeats,numFerns,fernSize;

  // parameters
  detThr = prms->fun_get_threshold();  // detection threshold
  objSize = prms->fun_get_object_size();  // object size
  numFeats = prms->fun_get_num_features();  // num. fern features
  numFerns = prms->fun_get_num_ferns();  // num. random ferns
  numPoses = prms->fun_get_num_poses();  // num. classifier poses
  imgShift = prms->fun_get_image_shift();  // image shift
  numSamples = prms->fun_get_num_train_samples();  // num. training samples

  // initialize the classifier
  clfr->fun_set_threshold(detThr);  // set threshold
  clfr->fun_compute(numFerns, numFeats, numPoses, objSize);  // compute random ferns
  clfr->fun_print();  // print classifier parameters

  // train the classifier for multiple poses -rotations-
  for (int pose = 1; pose <= numPoses; pose++){

    // update with positive samples
    fun_update_positive_samples(clfr, img, box, pose, numSamples, imgShift);

    // update with negative samples
    fun_update_negative_samples(clfr, img, box, pose, numSamples, imgShift);
  }

  // save classifier
  //clfr->fun_save();

};



