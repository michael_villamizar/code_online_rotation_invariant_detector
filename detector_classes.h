#ifndef objectDetector_h
#define objectDetector_h

#include <list>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

using namespace std;

// program parameters
class clsParameters{
  private:
    int imgWidth;  // image width
    int imgHeight;  // image height
    int objSize;  // object size (squared)
    int numFerns;  // num. random ferns
    int numFeatures;  // num. features per fern
    float threshold;  // classifier threshold
    float omega;  // assistance threshold
    int recThick;  // detection thickness (e.g rectangle)
    int numPoses;  // num. object poses (orientations)
    int numTrnSamples;  // num. training samples
    int numNewSamples;  // num. new (bootstrapped) samples
    float imgShift;  // image shift factor
    int minCellSize;  // min. cell size
    int maxCellSize;  // max. cell size
    CvScalar color;  // default color
    float font;  // text font
    int patchSize;  // patch size
    int imgEqualization;  // image equalization
    int saveImages;  // save images
    char const* filePath;  // file parameters path
public:
    clsParameters();  // constructor
    ~clsParameters();  // destructor
    void fun_initialize();  // initialize variables
    void fun_load();  // load parameters from file
    void fun_print();  // print program parameters
    int fun_get_image_width();  // return image width
    int fun_get_image_height();  // return image height
    int fun_get_object_size();  // return object size
    int fun_get_num_ferns();  // return num. random ferns
    int fun_get_num_features();  // return num. features
    int fun_get_num_poses();  // return the number of object poses
    float fun_get_threshold();  // return the classifier threshold
    float fun_get_image_shift();  // return the image shift rate
    int fun_get_num_train_samples();  // return the number of training samples
    int fun_get_num_new_samples();  // return the number of new bootstrapped samples
    int fun_get_min_cell_size();  // get min. cell size
    int fun_get_max_cell_size();  // get max. cell size
    int fun_get_patch_size();  // patch size
    float fun_get_font();  // return the text font
    int fun_get_rectangle_thickness();  // return rectangle thickness
    int fun_image_equalization();  // image equalization
    int fun_save_images();  // save images
    CvScalar fun_get_color();  // return rectangle color
    float fun_get_assistance_threshold();  // return the assistance threshold (omega)
    void fun_set_threshold(float thr);  // set the detection threshold
    void fun_set_num_new_images(int num);  // set the num. images for updating
    void fun_set_parameters_path(char* path);  // set the parameters file path
};


// classifier: online random ferns
class clsClassifier{
  private:
    int objSize;  // object size (squared)
    int numBins;  // number of histogram bins
    int numFerns;  // num. shared random ferns
    int numFeats;  // num. binary features per fern
    int numPoses;  // number of object poses
    int numChans;  // number of feature channels
    float threshold;  // threshold
    char* filePath;  // file parameters path
    cv::Mat ferns;  // random ferns data
    cv::Mat posHstms;  // positive fern histograms
    cv::Mat negHstms;  // negative fern histograms
    cv::Mat ratHstms;  // ratio of fern histograms
  public:
    clsClassifier();  // constructor
    ~clsClassifier();  // destructor
    void fun_save();  // save classifier in disk
    void fun_load();  // load classifier from disk
    void fun_print();  // print classifier parameters
    void fun_initialize();  // initialize
    void fun_update(cv::Mat& img, float label);  // update the object classifier
    void fun_compute(int numFerns, int numFeats, int numPoses, int objSize);  // compute classifier
    int fun_get_num_bins();  // return the number of histogram bins
    int fun_get_num_poses();  // return the number of object poses
    int fun_get_num_ferns();  // return the number of random ferns
    int fun_get_object_size();  // return object size
    float fun_get_threshold();  // return classifier threshold
    int fun_get_num_features();  // return the number of fern features
    int fun_get_num_channels();  // return the number of feature channels
    cv::Mat fun_get_ferns();  // return pointer to random ferns data
    cv::Mat fun_get_posHstms();  // return pointer to positive fern distributions
    cv::Mat fun_get_negHstms();  // return pointer to negative fern distributions
    cv::Mat fun_get_ratHstms();  // return pointer to ratio of fern distributions
    void fun_set_threshold(float thr);  // set detector threshold
    void fun_set_detector_path(char* path);  // set the detector file path
};


// one instance detection
class clsDetection{
  private:
    int pose;  // detection pose
    int scale;  // detection scale
    float score;  // detection score
    int x1,y1,x2,y2;  // detection location
  public:
    clsDetection();  // constructor
    ~clsDetection();  // destructor
    void fun_initialize();  // initialize
    void fun_set_values(int x1, int y1, int x2, int y2, float score, int pose, int scale);  // set detection values
    void fun_get_values(int &x1, int &y1, int &x2, int &y2, float &score, int &pose, int &scale);  // return detection values
};


// detection set -multiple detections-
class clsDetectionSet{
  private:
    int numDets;  // number of detections
    int numMaxDets;  // number muximum of detections
    clsDetection* det;  // array of detections
  public:
    clsDetectionSet();  // constructor
    ~clsDetectionSet();  // destructor
    void fun_release();  // release memory
    void fun_initialize();  // initialize
    int fun_get_num_detections();  // return number of detections
    int fun_get_num_max_detections();  // return number muximum of detections
    void fun_non_maxima_supression();  // return maxima detections
    void fun_scaling(float scaleFactor);  // scale detection coordinates
    void fun_set_num_detections(int value);  // set number of detections
    clsDetection* fun_get_detection(int index);  // return indexed detection
    void fun_remove_detections(clsDetection* detection);  // remove detections using overlapping measure
    void fun_add_detections(clsDetectionSet* detections);  // add new detections
    void fun_get_max_detection(clsDetection* maxDetection);  // return the maximun detection
    void fun_set_detection(clsDetection* detection, int index);  // set indexed detection
};


// integral image
class clsII{
  private:
    int width;  // integral image width
    int height;  // integral image height
    int cellSize;  // cell size -pixels x pixels-
    int imgWidth;  // image width
    int imgHeight;  // image height
    int numChannels;  // number of image channels : i.e color
    cv::Mat II;  // integral image
    cv::Mat img;  // image data
  public:
    clsII();  // constructor
    ~clsII();  // destructor
    cv::Mat fun_get_image();  // return the image pointer
    void fun_release_image();  // release image
    void fun_compute_image(int size);  // compute image from II
    void fun_integral_image(cv::Mat img);  // compute integral image
    void fun_get_image_size(int& sx, int& sy);  // return image size
};

// frame data
class clsFrameData{
  public:
    clsFrameData();  // constructor
    ~clsFrameData();  // destructor
    list<int> poses;  // detection poses
    list<CvRect> boxes;  // bounding boxes
    list<int> scales;  // detection scales
};

// detection ROI (region of interest)
class clsDetROI{
  private:
    int x;  // ROI box: y
    int y;  // ROI box: x
    int scale;  // scale
    int width;  // ROI box: width
    int height;  // ROI box: height
    bool active;  // ROI status flag
  public:
    clsDetROI();  // constructor
    ~clsDetROI();  // destructor
    void fun_clear();  // clear valeus
    int fun_get_scale();  // return ROI scale
    bool fun_get_status();  // return the ROI satus
    void fun_scale_box(float factor);  // scale ROI box
    void fun_copy_values(clsDetROI ROI);  // copy ROI values
    void fun_set_values(CvRect box, int scale);  // set ROI values
    void fun_set_box(int x, int y, int width, int height);  // set ROI box
    void fun_get_box(int &x, int &y, int &width, int &height);  // get ROI box
};

#endif


